from typing import Optional

from sqlalchemy.orm import Query

from domain.pre_register.model.pre_register_person import PreRegisterPerson
from domain.pre_register.port.secondary.pre_register_repository_port import PreRegisterRepositoryPort
from secondary.sqlalchemy.dbo.pre_register_player_dbo import to_dbo, PreRegisterPersonDBO
from secondary.sqlalchemy.session import session


class PreRegisterRepositoryAdapter(PreRegisterRepositoryPort):
    def __init__(self):
        pass

    def save(self, pre_register_person: PreRegisterPerson) -> PreRegisterPerson:
        pre_register_person_dbo: PreRegisterPersonDBO = to_dbo(pre_register_person)

        try:
            session.add(pre_register_person_dbo)
            session.commit()
            session.refresh(pre_register_person_dbo)
            return pre_register_person_dbo.to_model()
        except Exception as error:
            print(error)
            session.rollback()
        finally:
            session.close()

    def find_by_email(self, email: str) -> Optional[PreRegisterPerson]:
        pre_register_person_dbo: PreRegisterPersonDBO = session.query(PreRegisterPersonDBO).filter_by(email=email).first()
        return pre_register_person_dbo.to_model() if pre_register_person_dbo is not None else None
