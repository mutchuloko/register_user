import uuid

from sqlalchemy import Column, String, Date

from domain.pre_register.model.pre_register_person import PreRegisterPerson
from secondary.sqlalchemy.dbo.base_dbo import BaseDBO


class PreRegisterPersonDBO(BaseDBO):
    __tablename__: str = "pre_register_person"

    name = Column(String)
    birth_date = Column(Date)
    email = Column(String, unique=True)
    country = Column(String)
    password = Column(String)

    def to_model(self) -> PreRegisterPerson:
        return PreRegisterPerson(
            id=uuid.UUID(self.id),
            name=self.name,
            birth_date=self.birth_date,
            email=self.email,
            country=self.country,
            password=self.password
        )


def to_dbo(pre_register_player: PreRegisterPerson) -> PreRegisterPersonDBO:
    return PreRegisterPersonDBO(
        id=str(uuid.uuid4()) if pre_register_player.id is None else str(pre_register_player.id),
        name=pre_register_player.name,
        birth_date=pre_register_player.birth_date,
        email=pre_register_player.email,
        country=pre_register_player.country,
        password=pre_register_player.password
    )