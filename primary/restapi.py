from typing import List

import pinject
from falcon import API
from shared.singleton import Singleton


class RestApi(metaclass=Singleton):
    def __init__(self, middleware: list = None):
        self.__api = API(middleware=middleware)

    @property
    def api(self):
        return self.__api

    @staticmethod
    def init_controller(binding_specs: List = None, controllers: List = None):
        obj_graph = pinject.new_object_graph(binding_specs=binding_specs)

        for controller in controllers:
            try:
                obj_graph.provide(controller)
            except RuntimeError as error:
                raise error

