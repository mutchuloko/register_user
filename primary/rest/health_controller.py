from primary.rest.base_controller import BaseController
from primary.restapi import RestApi


class HealthController(BaseController):
    def on_get(self, req, resp):
        is_ok = {"status": "OK"}

        resp.media = is_ok

    def routes(self):
        RestApi().api.add_route("/health", self)
