from uuid import UUID
from datetime import date, datetime
from typing import Optional

from pydantic import validator, EmailStr, Field
from pydantic.main import BaseModel

from domain.pre_register.model.pre_register_person import PreRegisterPerson


class PreRegisterPersonInputDTO(BaseModel):
    name: Optional[str] = None
    email: Optional[EmailStr] = None
    birth_date: Optional[date] = Field(None, alias="birthdate")
    country: Optional[str] = None
    password: Optional[str] = Field(None, min_length=8)

    def to_model(self) -> PreRegisterPerson:
        return PreRegisterPerson(**self.__dict__)

    @validator("birth_date", pre=True)
    def birth_date_must_formatted(cls, v):
        if isinstance(v, str):
            try:
                return datetime.strptime(v, "%d/%m/%Y")
            except Exception as err:
                raise ValueError("birth_date does not match format '%d/%m/%Y", err)
        return v.title()


class PreRegisterPersonOutputDTO(BaseModel):
    id: Optional[UUID] = None
    name: Optional[str] = None
    email: Optional[EmailStr] = None
    birth_date: Optional[str] = Field(None, alias="birthdate")
    country: Optional[str] = None
    password: Optional[str] = Field(None, min_length=8)
    status: str

    @validator("birth_date", pre=True)
    def birth_date_must_formatted(cls, v):
        if isinstance(v, date):
            try:
                return date.strftime(v, "%d/%m/%Y")
            except Exception as err:
                raise ValueError("birth_date does not match format '%d/%m/%Y", err)
        return v.title()


def to_dto(pre_register_person: PreRegisterPerson) -> PreRegisterPersonOutputDTO:
    return PreRegisterPersonOutputDTO(**pre_register_person.to_dict)
