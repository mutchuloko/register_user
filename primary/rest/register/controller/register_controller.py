from falcon import Request, Response

from domain.pre_register.model.pre_register_person import PreRegisterPerson
from domain.pre_register.port.primary.pre_register_port import PreRegisterPort
from primary.rest.base_controller import BaseController
from primary.rest.register.dto.pre_register_player_dto import PreRegisterPersonInputDTO, to_dto
from primary.restapi import RestApi


class RegisterController(BaseController):

    def __init__(self, pre_register_port: PreRegisterPort):
        super().__init__()
        self.__pre_register_port = pre_register_port

    def on_post(self, req: Request, resp: Response):
        pre_register_person_input_dto = PreRegisterPersonInputDTO(**req.media)
        pre_register_person: PreRegisterPerson = self.__pre_register_port.save(pre_register_person_input_dto.to_model())
        resp.body = to_dto(pre_register_person).json()

    def on_get(self, req: Request, resp: Response, email: str):
        pre_register_person = self.__pre_register_port.find_by_email(email)
        resp.body = to_dto(pre_register_person).json()

    def routes(self):
        RestApi().api.add_route("/register", self)
        RestApi().api.add_route("/register/{email}", self)
