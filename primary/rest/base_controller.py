from abc import ABC, abstractmethod


class BaseController(ABC):
    def __init__(self):
        self.routes()

    @abstractmethod
    def routes(self):
        raise NotImplementedError
