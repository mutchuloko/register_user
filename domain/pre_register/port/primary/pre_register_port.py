from abc import ABC, abstractmethod

from domain.pre_register.model.pre_register_person import PreRegisterPerson


class PreRegisterPort(ABC):
    @abstractmethod
    def save(self, pre_register_person: PreRegisterPerson) -> PreRegisterPerson:
        pass

    @abstractmethod
    def find_by_email(self, email: str) -> PreRegisterPerson:
        pass
