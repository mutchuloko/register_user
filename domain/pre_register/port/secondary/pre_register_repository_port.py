from abc import ABC, abstractmethod
from typing import Optional

from domain.pre_register.model.pre_register_person import PreRegisterPerson


class PreRegisterRepositoryPort(ABC):
    @abstractmethod
    def save(self, pre_register_player: PreRegisterPerson) -> PreRegisterPerson:
        pass

    @abstractmethod
    def find_by_email(self, email: str) -> Optional[PreRegisterPerson]:
        pass
