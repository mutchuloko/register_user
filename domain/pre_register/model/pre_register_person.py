from dataclasses import field
from datetime import date
from enum import Enum
from typing import Optional
from uuid import UUID

from pydantic.dataclasses import dataclass


class Status(str, Enum):
    enabled = "ENABLED"
    disabled = "DISABLED"


@dataclass
class PreRegisterPerson:
    id: Optional[UUID] = None
    name: Optional[str] = None
    birth_date: Optional[date] = None
    email: Optional[str] = None
    country: Optional[str] = None
    password: Optional[str] = None
    status: Status = field(default=Status.enabled)

    @property
    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "birthdate": self.birth_date,
            "email": self.email,
            "country": self.country,
            "password": self.password,
            "status": self.status
        }

