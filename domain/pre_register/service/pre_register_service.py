from datetime import datetime
from uuid import uuid4

from domain.pre_register.model.pre_register_person import PreRegisterPerson
from domain.pre_register.port.primary.pre_register_port import PreRegisterPort
from domain.pre_register.port.secondary.pre_register_repository_port import PreRegisterRepositoryPort


class PreRegisterService(PreRegisterPort):

    def __init__(self, pre_register_repository_port: PreRegisterRepositoryPort):
        self.__pre_register_repository_port = pre_register_repository_port

    def save(self, pre_register_person: PreRegisterPerson) -> PreRegisterPerson:
        if (saved := self.find_by_email(pre_register_person.email)) is not None:

            items_update = dict(filter(lambda elem: elem[1] is not None, pre_register_person.__dict__.items()))
            saved.__dict__.update(items_update)

            return self.__pre_register_repository_port.save(saved)

        return self.__pre_register_repository_port.save(pre_register_person)

    def find_by_email(self, email: str) -> PreRegisterPerson:
        pre_register_person: PreRegisterPerson = self.__pre_register_repository_port.find_by_email(email)

        return None if pre_register_person is None else pre_register_person
