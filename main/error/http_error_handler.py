from falcon import HTTPError


class HTTPErrorHandler(HTTPError):
    def __init__(self, status, errors=None, *args, **kwargs):
        self.errors = errors
        super().__init__(status, *args, **kwargs)

    def to_dict(self, *args, **kwargs) -> dict:
        ret = super().to_dict(*args, **kwargs)
        if self.errors is not None:
            ret['errors'] = self.errors

        return ret
