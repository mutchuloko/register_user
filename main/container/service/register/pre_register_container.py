from pinject import BindingSpec

from domain.pre_register.service.pre_register_service import PreRegisterService
from secondary.sqlalchemy.adapter.pre_register_repository_adapter import PreRegisterRepositoryAdapter


class RegisterContainer(BindingSpec):
    def configure(self, bind):
        bind("pre_register_port", to_class=PreRegisterService)
        bind("pre_register_repository_port", to_class=PreRegisterRepositoryAdapter)
