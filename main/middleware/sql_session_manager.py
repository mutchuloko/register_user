from dynaconf import settings
from falcon import HTTP_500, Request, Response
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import scoping

from main.error.http_error_handler import HTTPErrorHandler
from secondary.sqlalchemy.session import session


class SQLAlchemySessionManager:
    def __init__(self):
        self.__session = session
        self.__scoped = isinstance(self.__session, scoping.ScopedSession)

    def process_request(self, req: Request, resp: Response):
        """
        Handle post-processing of the response (after routing).
        """
        req.context['session'] = self.__session

    def process_response(self, req: Request, resp: Response, resource, req_succeeded):
        """
        Handle post-processing of the response (after routing).
        """
        session_context = req.context['session']

        if settings.DB_AUTOCOMMIT:
            try:
                session_context.commit()
            except SQLAlchemyError as error:
                session_context.rollback()
                raise HTTPErrorHandler(
                    status=HTTP_500,
                    errors=error.args,
                    title="Database Rollback Error"
                )

        if self.__scoped:
            print("Removing Context")
            session_context.remove()
        else:
            print("Closing Context")
            session_context.close()
